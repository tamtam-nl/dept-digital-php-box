## Dept Digital PHP box

Blazingly fast PHP development. For a more indepth guide of features read the readme of Valet+ https://github.com/weprovide/valet-plus . DDBox is a fork of Valet+ with fixes for several problems. This repository is forked instead of contributing to prevent the overhead of open source projects.

## Requirements
[Homebrew](https://brew.sh/) 

## Installation
1. Install or update [Homebrew](https://brew.sh/) to the latest version using brew update.  

2. Install PHP 7.1 using Homebrew:  
`brew install homebrew/php/php71`


3. Install [Composer](https://getcomposer.org/) using Homebrew:  
`brew install homebrew/php/composer`

4. Install DDBox with the following composer command:  
`composer global config repositories.satis composer https://satis.tamtam.nl && composer global require dept/dd_box`

5. Make sure the ~/.composer/vendor/bin directory is in your system's "PATH".  
Add export PATH="$PATH:$HOME/.composer/vendor/bin" to .bash_profile (for bash) or .zshrc (for zsh) depending on your shell (echo $SHELL)

6. Run the valet install command:  
`valet install`

7. Once Valet is installed, try pinging any *.dev domain on your terminal using a command such as ping tamtam.dev. If Valet is installed correctly you should see this domain responding on 127.0.0.1. If not you might have to restart your system. Especially when coming from the Dinghy (docker) solution.

# Useful commands

## Export whole database
`mysqldump -uroot -proot -v --all-databases > ddboxbackup.sql`

## Available commands
**Available commands:**  

`valet configure`  
Configure application connection settings

`valet db`     
Database commands (list/ls, create, drop, reset, open, import, reimport, export/dump)

`valet domain`  
Get or set the domain used for Valet sites

`valet drupal`  
Copy ssh key

`valet elasticsearch`  
Enable / disable Elasticsearch

`valet fetch-share-url`  
Get the URL to the current Ngrok tunnel

`valet forget`  
Remove the current working (or specified) directory from Valet's list of paths

`valet help`  
Displays help for a command

`valet install`  
Install the Valet services

`valet ioncube`  
Enable / disable ioncube

`valet link`  
Link the current working directory to Valet

`valet links`  
Display all of the registered Valet links

`valet list`  
Lists commands

`valet log`  
Copy ssh key

`valet on-latest-version`  
Determine if this is the latest version of Valet

`valet open`     
Open the site for the current (or specified) directory in your browser

`valet park`  
Register the current working (or specified) directory with Valet

`valet paths`  
Get all of the paths registered with Valet

`valet phpstorm`  
Open closest git project in PHPstorm

`valet restart`  
Restart the Valet services

`valet secure`  
Secure the given domain with a trusted TLS certificate

`valet share`  
Generate a publicly accessible URL for your project

`valet ssh-key`  
Copy ssh key

`valet start`  
Start the Valet services

`valet stop`  
Stop the Valet services

`valet subdomain`  
Manage subdomains

`valet tower`  
Open closest git project in Tower

`valet uninstall`  
Uninstall the Valet services

`valet unlink`  
Remove the specified Valet link

`valet unsecure`  
Stop serving the given domain over HTTPS and remove the trusted TLS certificate

`valet use`  
Switch between versions of PHP

`valet vscode`  
Open closest git project in Visual Studio Code

`valet which`  
Determine which Valet driver serves the current working directory

`valet xdebug`  
Enable / disable Xdebug